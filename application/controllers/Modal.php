<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modal extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('ModalModel', 'modal');
    }

    public function index(){
        $data['modal'] = $this->modal->modal();
        $data['modal2'] = $this->modal->modal2();
        $data['titulo'] = "Modal";
        $this->show('modal', $data);
    }
}


?>