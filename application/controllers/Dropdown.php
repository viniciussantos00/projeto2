<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dropdown extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('DropdownModel','dropdown');
    }

    public function index(){
        $data['dropdown'] = $this->dropdown->dropdown();
        $data['dropdown2'] = $this->dropdown->dropdown2();
        $data['dropup'] = $this->dropdown->dropup();
        $data['titulo'] = "Dropdown";
        $this->show('Dropdown', $data);
    }
}


?>