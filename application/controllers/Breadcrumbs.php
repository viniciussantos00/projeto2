<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Breadcrumbs extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('BreadcrumbsModel', 'breadcrumbs');
    }

    public function index(){
        $data['breadcrumbs'] = $this->breadcrumbs->breadcrumbs();
        $data['breadcrumbs2'] = $this->breadcrumbs->breadcrumbs2();
        $data['titulo'] = "Breadcrumbs";
        $this->show('Breadcrumbs', $data);
    }
}

?>