<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. '/controllers/test/MyToast.php');

class TestComponentBreadcrumbs extends MyToast{

    function __construct(){
        parent::__construct('TestComponentBreadcrumbs');
        $data['titulo'] = "Teste Breadcrumbs";
        $this->load->view('common/topo', $data);
        $this->load->view('test');
        $this->load->view('common/navbar');
    }
        
    function test_br_type(){
        $br_type = 'breadcrumb purple lighten-4';

        $this->_assert_not_empty($br_type, "A variável está vazia!");
    }

    function test_li(){
        $li = '<li class="breadcrumb-item"><a href="#">Home</a></li>';

        $this->_assert_not_empty($li, "A variável está vazia!");
    }

    function test_li2(){
        $li2 = '<li class="breadcrumb-item"><a href="#">Library</a></li>';

        $this->_assert_not_empty($li2, "A variável está vazia!");
    }

    function test_liActive(){
        $liActive = '<li class="breadcrumb-item active">Data</li>';

        $this->_assert_not_empty($liActive, "A variável está vazia!");
    }

}?>