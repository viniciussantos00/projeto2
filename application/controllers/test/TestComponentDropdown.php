<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. '/controllers/test/MyToast.php');

class TestComponentDropdown extends MyToast{

    function __construct(){
        parent::__construct('TestComponentDropdown');
        $data['titulo'] = "Teste Dropdown";
        $this->load->view('common/topo', $data);
        $this->load->view('test');
        $this->load->view('common/navbar');
    }

    function test_botao(){
        $botao = 'btn btn-secondary dropdown-toggle';
        
        $this->_assert_not_empty($botao, "A variável está vazia!");
    }

    function test_titulo_bt(){
        $titulo_bt = 'Dropdown Menu';

        $this->_assert_not_empty($titulo_bt, "A variável está vazia!");
    }

    function test_menu_type(){
        $menu_type = 'dropdown';

        $this->_assert_not_empty($menu_type, "A variável está vazia!");
    }
    function test_li(){
        $li = '<li><a href="#">HTML</a></li>';

        $this->_assert_not_empty($li, "A variável está vazia!");
    }
    function test_li2(){
        $li2 = '<li><a href="#">CSS</a></li>';

        $this->_assert_not_empty($li2, "A variável está vazia!");
    }
    function test_li3(){
        $li3 = '<li><a href="#">JavaScript</a></li>';

        $this->_assert_not_empty($li3, "A variável está vazia!");
    }
    function test_liHeader(){
        $liHeader = ' ';

        $this->_assert_not_empty($liHeader, "A variável está vazia!");
    }
}

?>