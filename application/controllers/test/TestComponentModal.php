<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH. '/controllers/test/MyToast.php');

class TestComponentModal extends MyToast{

    function __construct(){
        parent::__construct('TestComponentModal');
        $data['titulo'] = "Teste Modal";
        $this->load->view('common/topo', $data);
        $this->load->view('test');
        $this->load->view('common/navbar');
    }
        
    function test_body(){
        $body = 'Conteúdo da caixa.';

        $this->_assert_not_empty($body, "A variável está vazia!");
    }

    function test_botao(){
        $botao = 'btn btn-primary';

        $this->_assert_not_empty($botao, "A variável está vazia!");
    }

    function test_ct_botao(){
        $ct_botao = 'Modal Bottom';

        $this->_assert_not_empty($ct_botao, "A variável está vazia!");
    }

    function test_footer(){
        $footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';

        $this->_assert_not_empty($footer, "A variável está vazia!");
    }

    function test_modal_type(){
        $modal_type = 'modal-frame modal-bottom';

        $this->_assert_not_empty($modal_type, "A variável está vazia!");
    }

    function test_modal_transition(){
        $modal_transition = 'modal fade bottom';

        $this->_assert_not_empty($modal_transition, "A variável está vazia!");
    }

    function test_modal_title(){
        $modal_title = 'Teste';

        $this->_assert_not_empty($modal_title, "A variável está vazia!");
    }

    function test_idButton(){
        $idButton = 'modal2';

        $this->_assert_not_empty($idButton, "A variável está vazia!");
    }

    function test_id(){
        $id = 'id2';

        $this->_assert_not_empty($id, "A variável está vazia!");
    }
}
?>