<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/components/Modal.php';

class ModalModel extends CI_Model {

    public function modal(){

        $body = 'Conteúdo da caixa de diálogo.';
        $botao = 'btn btn-primary';
        $ct_botao = 'Tenha uma surpresa ao clicar';
        $footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $modal_type = 'modal-lg';
        $modal_transition = 'modal fade right';
        $modal_title = 'Teste Modal';
        $idButton = 'basicExampleModal';
        $id = 'exampleModalLabel';

        $modal = new ModalC($body, $botao, $ct_botao, $footer, $modal_type, $modal_title, $modal_transition, $idButton, $id);

        return $modal->getHTML();
    }

    public function modal2(){
        $body = 'Conteúdo da caixa.';
        $botao = 'btn btn-primary';
        $ct_botao = 'Modal Bottom';
        $footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $modal_type = 'modal-frame modal-bottom';
        $modal_transition = 'modal fade bottom';
        $modal_title = 'Teste';
        $idButton = 'modal2';
        $id = 'id2';

        $modal2 = new ModalC($body, $botao, $ct_botao, $footer, $modal_type, $modal_title, $modal_transition, $idButton, $id);

        return $modal2->getHTML();
    }
}

?>