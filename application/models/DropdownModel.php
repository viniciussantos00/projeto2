<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/components/Dropdown.php';

class DropdownModel extends CI_Model {

    public function dropdown(){

        $botao = 'btn btn-secondary dropdown-toggle';
        $titulo_bt = 'Dropdown Menu';
        $menu_type = 'dropdown';
        $li = '<li><a href="#">HTML</a></li>';
        $li2 = '<li><a href="#">CSS</a></li>';
        $li3 = '<li><a href="#">JavaScript</a></li>';
        $liHeader = '';

        $dropdown = new DropdownC($botao, $titulo_bt, $menu_type, $li, $li2, $li3, $liHeader);

        return $dropdown->getHTML();
    }

    public function dropdown2(){
        $botao = 'btn btn-secondary dropdown-toggle';
        $titulo_bt = 'Dropdown Menu';
        $menu_type = 'dropdown';
        $li = '<li><a href="#">HTML</a></li>';
        $li2 = '<li><a href="#">CSS</a></li>';
        $li3 = '<li><a href="#">JavaScript</a></li>';
        $liHeader = '<li class="dropdown-header">Cabeçalho</li>';

        $dropdown2 = new DropdownC($botao, $titulo_bt, $menu_type, $li, $li2, $li3, $liHeader);

        return $dropdown2->getHTML();
    }

    public function dropup(){
        $botao = 'btn btn-secondary dropdown-toggle';
        $titulo_bt = 'Dropup Menu';
        $menu_type = 'dropup';
        $li = '<li><a href="#">HTML</a></li>';
        $li2 = '<li><a href="#">CSS</a></li>';
        $li3 = '<li><a href="#">JavaScript</a></li>';
        $liHeader = '<li class="dropdown-header">Cabeçalho</li>';

        $dropup = new DropdownC($botao, $titulo_bt, $menu_type, $li, $li2, $li3, $liHeader);

        return $dropup->getHTML();
    }
}

?>