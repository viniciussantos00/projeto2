<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/components/Breadcrumbs.php';

class BreadcrumbsModel extends CI_Model {

    public function breadcrumbs(){
        
        $br_type = 'breadcrumb purple lighten-4';
        $li = '<li class="breadcrumb-item"><a href="#">Home</a></li>';
        $li2 = '<li class="breadcrumb-item"><a href="#">Library</a></li>';
        $liActive = '<li class="breadcrumb-item active">Data</li>';

        $breadcrumbs = new BreadcrumbsC($br_type, $li, $li2, $liActive);

        return $breadcrumbs->getHTML();
    }

    public function breadcrumbs2(){

        $br_type = 'breadcrumb secondary-color';
        $li = '<li class="breadcrumb-item"><a class="white-text" href="#">Home</a></li>';
        $li2 = '<li class="breadcrumb-item"><a class="white-text" href="#">Library</a></li>';
        $liActive = '<li class="breadcrumb-item active">Data</li>';

        $breadcrumbs2 = new breadcrumbsC($br_type, $li, $li2, $liActive);

        return $breadcrumbs2->getHTML();
    }
}


?>