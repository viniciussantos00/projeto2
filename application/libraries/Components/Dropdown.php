<?php
  /**
   * Esta é a classe do componente Dropdown
   * 
   * Aqui foi separado em funções a construção do componente, declarando todas as variáveis de forma privada
   * para que não ocorra o acesso direto a elas, assim como a função que está montando o componente.
   * Desta forma, o único acesso direto possível é via função "getHTML()" onde esta irá acessar as outras.
   * 
   * @author Vinicius dos Santos
   * 
   * Funções utilizadas
   * 
   * @param $botao             disponibiliza o botão para acessar o menu
   * @param $titulo_bt         recebe o nome a ser exibido no botão
   * @param $menu_type         especifica o estilo do menu
   * @param $li                opção para uma página
   * @param $li2               opção para uma página
   * @param $li3               opção para uma página
   * @param $liHeader          cabeçalho exibido dentro do menu
   * 
   * Para adicionar mais uma página, basta acrescentar mais um $li(n) no componente
   * 
   * @copyright 2019 Vinicius dos Santos
   */

    include_once 'Component.php';

    class DropdownC extends Component {

        private $botao;
        private $titulo_bt;
        private $menu_type;
        private $li;
        private $li2;
        private $li3;
        private $liHeader;

        public function __construct($botao, $titulo_bt, $menu_type, $li, $li2, $li3, $liHeader){
            $this->botao = $botao;
            $this->titulo_bt = $titulo_bt;
            $this->menu_type = $menu_type;
            $this->li = $li;
            $this->li2 = $li2;
            $this->li3 = $li3;
            $this->liHeader = $liHeader;
        }

        public function getHTML(){
            $html = $this->dropdown();
            return $html;
        }

        private function dropdown(){

            $html = '<div class="'.$this->menu_type.'">
            <button class="'.$this->botao.'" type="button" data-toggle="dropdown">'.$this->titulo_bt.'
            <span class="caret"></span></button>
            <ul class="dropdown-menu">
              '.$this->liHeader.'
              '.$this->li.'
              '.$this->li2.'
              '.$this->li3.'
            </ul>
            </div>';

            return $html;
        }
    }

?>