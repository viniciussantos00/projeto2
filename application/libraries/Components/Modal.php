<?php
  /**
   * Esta é a classe do componente Modal
   * 
   * Aqui foi separado em funções a construção do componente, declarando todas as variáveis de forma privada
   * para que não ocorra o acesso direto a elas, assim como a função que está montando o componente.
   * Desta forma, o único acesso direto possível é via função "getHTML()" onde esta irá acessar as outras.
   * 
   * @author Vinicius dos Santos
   * 
   * Funções utilizadas
   * 
   * @param $body               recebe o conteúdo do modal
   * @param $botao              disponibiliza o botão para acessar o modal
   * @param $ct_botao           recebe o nome a ser exibido no botão
   * @param $footer             exibe o footer da modal
   * @param $modal_type         especifica o estilo do modal
   * @param $modal_transition   indica o tipo de transição que o modal utilizará
   * @param $modal_title        exibe o título da caixa de diálogo do modal
   * @param $id                 contém o link de conexão do título com a caixa de diálogo
   * @param $idButton           contém o link de conexão do botão com a caixa de diálogo
   * 
   * @copyright 2019 Vinicius dos Santos
   */

    include_once 'Component.php';

    class ModalC extends Component {

        private $body;
        private $botao;
        private $ct_botao;
        private $footer;
        private $modal_type;
        private $modal_transition;
        private $modal_title;
        private $id;
        private $idButton;

        public function __construct($body, $botao, $ct_botao, $footer, $modal_type, $modal_title, $modal_transition, $idButton, $id){
            $this->body = $body;
            $this->botao = $botao;
            $this->ct_botao = $ct_botao;
            $this->footer = $footer;
            $this->modal_type = $modal_type;
            $this->modal_title = $modal_title;
            $this->modal_transition = $modal_transition;
            $this->idButton = $idButton;
            $this->id = $id;
        }

        public function getHTML(){
            $html = $this->modal().'</div>
            </div>
           </div>
         </div>';

            return $html;
        }

        private function modal(){
          $html = '<button type="button" class="'.$this->botao.'" data-toggle="modal" data-target="#'.$this->idButton.'">
          '.$this->ct_botao.'</button>';

            $html .= '<div class="'.$this->modal_transition.'" id="'.$this->idButton.'" tabindex="-1" role="dialog" aria-labelledby="'.$this->id.'" aria-hidden="true">
            <div class="modal-dialog '.$this->modal_type.'" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="'.$this->id.'">'.$this->modal_title.'</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>';
                $html .= '<div class="modal-body">'.$this->body.'</div>';
                $html .= '<div class="modal-footer">'.$this->footer.'';
                return $html;
        }
    }
?>