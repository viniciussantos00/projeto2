<?php
   /**
   * Esta é a classe do componente Dropdown
   * 
   * Aqui foi separado em funções a construção do componente, declarando todas as variáveis de forma privada
   * para que não ocorra o acesso direto a elas, assim como a função que está montando o componente.
   * Desta forma, o único acesso direto possível é via função "getHTML()" onde esta irá acessar as outras.
   * 
   * @author Vinicius dos Santos
   * 
   * Funções utilizadas
   * 
   * @param $br_type           define o tipo do breadcrumb
   * @param $li                opção para uma página
   * @param $li2               opção para uma página
   * @param $liActive          define a página "ativa"
   * 
   * Para adicionar mais uma página, basta acrescentar mais um $li(n) no componente
   * 
   * @copyright 2019 Vinicius dos Santos
   */
    include_once 'Component.php';

    class BreadcrumbsC extends Component {

        private $br_type;
        private $li;
        private $li2;
        private $liActive;

        public function __construct($br_type, $li, $li2, $liActive){
            $this->br_type = $br_type;
            $this->li = $li;
            $this->li2 = $li2;
            $this->liActive = $liActive;
        }

        public function getHTML(){
            $html = $this->Breadcrumbs();
            return $html;
        }

        private function Breadcrumbs(){
            
            $html = '<nav aria-label="breadcrumb">
                        <ol class="'.$this->br_type.'">
                            '.$this->li.'
                            '.$this->li2.'
                            '.$this->liActive.'
                        </ol>
                    </nav>';

          return $html;
        }
    }
?>