<hr class="my-5">

<section class="wow fadeIn">

  <h2 class="h1 text-center my-5 font-weight-bold">Projeto de Linguagem de Programação II</h2>

  <div class="row text-left">

    <div class="col-lg-6 col-md-12">
      <div class="view overlay rounded z-depth-1-half mb-3">
        <img src="https://mdbootstrap.com/img/Photos/Others/images/77.jpg" class="img-fluid" alt="Sample post image">
        <a href="<?php echo base_url('breadcrumbs');?>">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>
      <h3>
        <a href="<?php echo base_url('breadcrumbs');?>">
          <strong> Breadcrumbs </strong>
        </a>
      </h3>
      <p> Um componente que indica a localização da página atual na hierarquia de navegação </p>
      <hr>
    </div>

    <div class="col-lg-6 col-md-12">
      <div class="view overlay rounded z-depth-1-half mb-3">
        <img src="https://mdbootstrap.com/img/Photos/Others/images/32.jpg" class="img-fluid" alt="Sample post image">
        <a href="<?php echo base_url('dropdown');?>">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>
      <h3>
        <a href="<?php echo base_url('dropdown');?>">
          <strong> Dropdown </strong>
        </a>
      </h3>
      <p>Um componente que cria um menu </p>
      <hr>

    </div>
    
    <div class="col-lg-6 col-md-12">
      <div class="view overlay rounded z-depth-1-half mb-3">
        <img src="https://mdbootstrap.com/img/Photos/Others/images/3.jpg" class="img-fluid" alt="Sample post image">
        <a href="<?php echo base_url('modal');?>">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>
      <h3>
        <a href="<?php echo base_url('modal');?>">
          <strong> Modal </strong>
        </a>
      </h3>
      <p> Um componente que abre uma caixa de diálogo/janela popup </p>

      </div>
    </div>
  </div>
</section>
</div>
</main>