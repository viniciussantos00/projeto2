<div style="margin-top:100px; margin-right:50px; margin-left:50px;">
    <h3 class = "text-center"> Dropdown </h3>
    <?= $dropdown ?><br/>
    <p>O dropdown é um componente que cria um menu (não somente dropdown, como veremos mais a frente).</p>  
    <h4>Código do componente: </h4><br/>
    <div class="container">
        <div class="col-md-6 ">
            <textarea readonly rows='7' cols='100' class="border border-success">
            <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">Dropdown Menu
            <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li><a href="#">HTML</a></li>
                <li><a href="#">CSS</a></li>
                <li><a href="#">JavaScript</a></li>
            </ul>
            </div>
            </textarea>
        </div>
    </div>
    <br/></br>

    <p>O dropdown pode ter um cabeçalho adicionando somente mais um li com a class de dropdown-header: </p><br/>
    <div class="container">
        <div class="col-md-6">
            <textarea class="border border-success" cols="40" rows="1"><li class="dropdown-header">Cabeçalho</li></textarea>
        </div>
    </div><br>
    <?=$dropdown2?><br/><br/>

    <p>A posição do menu também pode ser variável de acordo com as suas necessidades alterando a ul: </p><hr/>
    <div class="container">
        <div class="col-md-6 ">
            <textarea readonly rows='7' cols='100' class="border border-success">
            <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">Dropdown Menu
            <span class="caret"></span></button>
            <ul class="dropup">
                <li><a href="#">HTML</a></li>
                <li><a href="#">CSS</a></li>
                <li><a href="#">JavaScript</a></li>
            </ul>
            </div>
            </textarea>
        </div>
    </div><br/>
    <?=$dropup?>
    <br/><br/>

    <p>As variáveis utilizadas foram as seguintes: 
        <li>$botao: é variável que exibe o botão para abrir o menu</li>
        <li>$titulo_bt: define o nome que aparecerá no botão</li>
        <li>$menu_type: é o tipo de menu, como menu dropdown ou dropup</li>
        <li>$li: opção para uma página</li>
        <li>$li2: opção para outra página</li>
        <li>$li3: opção para outra página</li>
        <li>$liHeader: variável para definir um cabeçalho.</li>
    </p>
</div>