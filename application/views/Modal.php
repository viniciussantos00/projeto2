<div style="margin-top:100px; margin-right:50px; margin-left:50px;">
    <h3 class = "text-center"> Modal </h3>
    <?= $modal ?><br/><br/>
    <p>Modal é um componente que abre uma caixa de diálogo/janela popup que pode ser usada para diversos meios
       como notificação para o usuário, componentes de comércio online e etc.</p>
    <h4>Código do componente: </h4><br/>
    <div class="container">
        <div class="col-md-6 ">
            <textarea readonly rows='15' cols='100' class="border border-info">

<div class="modal fade" id="centralModalSm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <div class="modal-dialog modal-sm" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Modal title</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-sm">Save changes</button>
      </div>
    </div>
  </div>
</div>

            </textarea>
        </div>
    </div>
    <br/><br/>
    <p>Para criar este componente eu separei as variáveis de conteúdo, do botão, o tipo, transição e título da caixa de diálogo.
       Podemos deifinir o tamanho e como a caixa aparecerá para o usuário: </p><br/>
    <?=$modal2?>
    <br/><br/>

    <p>As variáveis utilizadas foram as seguintes: 
        <li>$body: contém o conteúdo exibido dentro da caixa de diálogo</li>
        <li>$botao: contém o tipo de botão utilizado para abrir a caixa</li>
        <li>$ct_botao: título do botão</li>
        <li>$footer: trata-se do footer do componente (botões da caixa)</li>
        <li>$modal_type: tipo de modal utilizado (tamanho e posição)</li>
        <li>$modal_transition: variável que indica a animação de exibição do modal</li>
        <li>$modal_title: título da caixa de diálogo</li>
        <li>$id: é a id que contém o link do título com a caixa</li>
        <li>$idButton: é a id que contém o link do botão com a caixa </li>
    </p>
</div>