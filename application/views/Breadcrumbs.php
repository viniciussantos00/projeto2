<div style="margin-top:100px; margin-right:50px; margin-left:50px">
    <h3 class = "text-center"> Breadcrumbs </h3><br/>
    <?= $breadcrumbs ?><br/>
    <p>Breadcrumb é um componente muito simples que serve para indicar a localização da página atual
       na hierarquia de navegação que adiciona separadores automaticamente via CSS.</p>
    <h4>Código do componente: </h4><br/>
    <div class="container">
        <div class="col-md-6 ">
            <textarea readonly rows='7' cols='100' class="border border-danger">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Library</a></li>
                    <li class="breadcrumb-item active">Data</li>
                </ol>
            </nav>
            </textarea>
        </div>
    </div>

    <br/><br/>
    <p>Para transformar este componente em uma classe foi criado algumas variáveis, que podem alterar o componente,
       dentro do model. Estas variáveis foram enviadas para a library que constrói o componente com a base do 
       html e incrementando com as variáveis definidas no model. Veja a seguir:</p><br/>
    <div class="container">
        <div class="col-md-6">
            <textarea readonly rows='10' cols='100' class="border border-danger">private function Breadcrumbs(){    
            $html = '<nav aria-label="breadcrumb">
                        <ol class="'.$this->br_type.'">
                            '.$this->li.'
                            '.$this->li2.'
                            '.$this->liActive.'
                        </ol>
                    </nav>';
            return $html;
            }
            </textarea>
        </div>
    </div>
    <br/><br/>

    <p>Desta forma, podemos alterar dentro do model como quisermos para termos formas diferentes do mesmo componente, tal qual: </p>
    <br/><?= $breadcrumbs2?><br/>
    <div class="container">
        <div class="col-md-6">
            <textarea readonly rows='10' cols='100' class="border border-danger">

            $br_type = 'breadcrumb secondary-color';
            $li = '<li class="breadcrumb-item"><a class="white-text" href="#">Home</a></li>';
            $li2 = '<li class="breadcrumb-item"><a class="white-text" href="#">Library</a></li>';
            $liActive = '<li class="breadcrumb-item active">Data</li>';

            $breadcrumbs2 = new breadcrumbsC($br_type, $li, $li2, $liActive);

            return $breadcrumbs2->getHTML();
            </textarea>
        </div>
    </div>
    <br/><br/>
    
    <p>As variáveis utilizadas foram as seguintes: 
        <li>$br_type: é onde se define o tipo do Breadcrumb</li>
        <li>$li: é onde se define as páginas</li>
        <li>$li2: definindo mais uma página</li>
        <li>$liActive: este por sua vez define qual página está "ativa"</li>
    </p>
</div>