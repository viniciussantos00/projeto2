<nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
      <div class="container">

        <a class="navbar-brand waves-effect" href="<?php echo base_url();?>">
          <strong class="blue-text">Vinicius</strong>
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link waves-effect" href="<?php echo base_url();?>">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="<?php echo base_url('breadcrumbs');?>">Breadcrumbs</a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="<?php echo base_url('dropdown');?>">Dropdown</a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="<?php echo base_url('modal');?>">Modal</a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="<?php echo base_url('test/TestComponentBreadcrumbs');?>">Teste Componente Breadcrumbs</a>
              <li class="nav-item">
              <a class="nav-link waves-effect" href="<?php echo base_url('test/TestComponentDropdown');?>">Teste Componente Dropdown</a>
              <li class="nav-item">
              <a class="nav-link waves-effect" href="<?php echo base_url('test/TestComponentModal');?>">Teste Componente Modal</a>
            </li>
          </ul>

        </div>

      </div>
    </nav>
    <!-- Navbar -->

  </header>