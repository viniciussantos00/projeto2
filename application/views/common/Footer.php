<!--Footer-->
<footer class="page-footer text-center font-small mdb-color darken-2 mt-4 wow fadeIn">

<hr class="my-4">

<div class="pb-4">
  <a href="https://www.facebook.com/" target="_blank">
    <i class="fab fa-facebook-f mr-3"></i>
  </a>

  <a href="https://www.instagram.com/" target="_blank">
    <i class="fab fa-instagram mr-3"></i>
  </a>
</div>

<div class="footer-copyright py-3">
  © 2019 Copyright: Vinicius dos Santos
</div>

</footer>
<!--/.Footer-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.4.1.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/mdb.min.js"></script>
  <script type="text/javascript">
    new WOW().init();

  </script>
</body>
</html>